package eu.smile.elasticsuite.ml.event

import eu.smile.elasticsuite.ml.config.EventProcessorConfig

import org.apache.spark.sql.SparkSession
import org.elasticsearch.spark.sql._
import org.apache.spark.sql.{Dataset,Row}

object EventDatasource {

    private val endDate   = EventProcessorConfig().endDate
    private val startDate = EventProcessorConfig().startDate

    def createDataFrame() : Dataset[Row] = {
        val spark = EventProcessorConfig().sparkSession
        import spark.implicits._
        spark.esDF("event/event")
          .withColumn("date", $"@timestamp".cast("date"))
          .filter($"date" >= startDate && $"date" <= endDate)
  }
}
