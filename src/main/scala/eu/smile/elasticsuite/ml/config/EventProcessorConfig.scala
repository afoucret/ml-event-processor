package eu.smile.elasticsuite.ml.config

import org.apache.spark.sql.SparkSession
import java.time.LocalDate

class EventProcessorConfig(private val options: Map[String, String]) {
    val esHost         = options.getOrElse("esHost", "localhost:9200")
    val eventIndex     = options.getOrElse("eventIndex", "event")
    val endDate        = options.getOrElse("date", LocalDate.now().minusDays(1).toString()).toString()
    val retentionDelay = options.getOrElse("retentionDelay", "30").toLong
    val startDate      = LocalDate.parse(endDate).minusDays(retentionDelay).toString()
    val outputDir      = options.getOrElse("outputDir", "output")

    val sparkSession   = initSession()
    private def initSession() : SparkSession = {
        SparkSession.builder.appName("ElasticSuite Event Processor")
            .config("es.nodes", esHost)
            .config("es.nodes.wan.only", true)
            .getOrCreate()
    }
}

object EventProcessorConfig {
  var config:EventProcessorConfig = null

  def apply() : EventProcessorConfig = {
      if (config != null) config else EventProcessorConfig.apply(Array[String]())
  }

  @throws(classOf[Exception])
  def apply(args: Array[String]) : EventProcessorConfig = {
      if (config != null) {
          throw new Exception("Config have been set already")
      }
      config = new EventProcessorConfig(parseConfig(args))
      config
  }

  private def parseConfig(args: Array[String]) : Map[String, String] = {
      parseNextOption(Map(), args.toList)
  }

  @throws(classOf[Exception])
  private def parseNextOption(map: Map[String, String], args: List[String]) : Map[String, String] = {
      args match {
          case Nil => map
          case "--esHost"  :: value :: tail => parseNextOption(map ++ Map("esHost" -> value), tail)
          case "--eventIndex" :: value :: tail => parseNextOption(map ++ Map("eventIndex" -> value), tail)
          case "--date" :: value :: tail => parseNextOption(map ++ Map("date" -> value), tail)
          case "--retentionDelay" :: value :: tail => parseNextOption(map ++ Map("retentionDelay" -> value), tail)
          case "--outputDir" :: value :: tail => parseNextOption(map ++ Map("outputDir" -> value), tail)
          case option :: Nil =>  throw new Exception("Unknon option : " + option)
          case option :: tail => throw new Exception("Unknon option : " + option)
      }
  }
}
