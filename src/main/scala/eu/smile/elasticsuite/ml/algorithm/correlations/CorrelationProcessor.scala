package eu.smile.elasticsuite.ml.algorithm.correlations

import eu.smile.elasticsuite.ml.algorithm.{Processor,Output}

import org.apache.spark.sql.{Dataset,Row}
import org.apache.spark.sql.functions._

class CorrelationProcessor() extends Processor {

    def process(source: Dataset[Row]) : Output = {

        import source.sparkSession.implicits._

        val queryHistory   = extractQueriesHistory(source)
        val productHistory = extractProductHistory(source)

        val correlations = queryHistory.join(productHistory, "session_id")
            .groupBy($"query", $"product_id")
            .count()
            .select($"query", $"product_id", $"count".as("correlations"))
            .cache()
            .filter($"correlations" > 3)
            .orderBy($"correlations".desc)

        new Output(
            Map(
                "found_correlations" -> correlations.count(),
                "query_history_size" -> queryHistory.count(),
                "product_history_size" -> productHistory.count()
            ),
            Map("correlations" -> correlations)
        )
    }

    private def extractQueriesHistory(source: Dataset[Row]) : Dataset[Row] = {
        import source.sparkSession.implicits._
        source.filter($"session.uid".isNotNull && $"page.search.query".isNotNull)
              .select($"session.uid".as("session_id"), $"page.search.query".as("query"))
              .distinct
    }

    private def extractProductHistory(source: Dataset[Row]) : Dataset[Row] = {
        import source.sparkSession.implicits._
        source.filter($"session.uid".isNotNull && $"page.product.id".isNotNull)
              .select($"session.uid".as("session_id"), $"page.product.id".as("product_id"))
              .distinct
    }
}
